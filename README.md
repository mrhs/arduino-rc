# arduino-rc

TODO
- Buy or find some batteries (Total 10 V) for power source
- Design the casing in order for the car to be mobile and steady parts
- Add ultrasonic sensor and fetch and show it's data on the android controller

This project consists of two parts:

- Arduino part
    - Uno Controller
        
        This part takes the command from wifi receiver and executes it.
        The command is in the form of X[speed]Y[direction] for example X5Y2.
        The range for speed is -5<X<5, -5 means backward with maximum power and 5 means foreward with maximum power and for -1<Y<1, -1 means left, 1 means right and 0 means forward. 
        There are 3 meain function in this part:
            
        - Serial manager
            
            Takes the command with the desired form from the Software serial and save it as a new command in Queue manager with 100 millisecond for execution time.
        - Queue manager
        
            Each time when setStep function is executed, the time passed from the last epoch is stored and is subtracted from the execution time of the first command in the queue. If it is lower or equal to zero, the first command is popped and next command if exists is replaced. The currentX and currentY are set accordingly.
        - Actuator manager
        
            This function takes the current X and Y from the Queue manager and sets the output pins accordingly to execute it.
    - Wemos D1 reciever
        
        In this part a wifi access point is created for the controller to connect to. A listener is set to a specific port for any incoming UDP packets. The data taken from the packet is checked and if it is valid, it is sent to Uno controller via Software serial.
        
    Because wemos D1 works on 3.3 volt and Uno on 5 V. In order to connect them, a voltage divider has been used with two 2.2k ohm and 1k ohm resistors.    
    
- Android app

    This part consists of two virtual joystick for controlling movement in two x and y axis.
    Every 100 milliseconds, if any joystick is pressed, a udp packet consisting the command is sent to the arduino part.    
    

